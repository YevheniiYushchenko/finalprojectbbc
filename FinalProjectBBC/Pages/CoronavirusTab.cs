﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectBBC.Pages
{
    public class CoronavirusTab : BasePage
    {
        private readonly string yourCoronavirusStoriesTabXPath 
            = "//nav[@class='nw-c-nav__wide-secondary']//a[@class='nw-o-link']";

        public CoronavirusTab(IWebDriver driver) : base(driver) { }

        public IWebElement YourCoronavirusStoriesTab 
            => driver.FindElement(By.XPath(yourCoronavirusStoriesTabXPath));
    }
}
