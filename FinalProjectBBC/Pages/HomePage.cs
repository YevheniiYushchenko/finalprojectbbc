﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace FinalProjectBBC.Pages
{
    public class HomePage : BasePage
    {
        private readonly string newsButtonXPath = "//div[@id='orb-header']//li[@class='orb-nav-newsdotcom']";

        public HomePage(IWebDriver driver) : base(driver) { }

        public IWebElement NewsButton => driver.FindElement(By.XPath(newsButtonXPath));
    }
}
