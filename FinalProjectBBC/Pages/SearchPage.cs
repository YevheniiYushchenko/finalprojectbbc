﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectBBC.Pages
{
    public class SearchPage : BasePage
    {
        private readonly string searchResultXPath = "//p/a";

        public SearchPage(IWebDriver driver) : base(driver) { }

        public string SearchResult 
            => driver.FindElement(By.XPath(searchResultXPath)).Text;
    }
}
