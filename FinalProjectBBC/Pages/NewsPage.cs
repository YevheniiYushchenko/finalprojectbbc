﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectBBC.Pages
{
    public class NewsPage : BasePage
    {
        private readonly string exitFromSingInMessageButtonXPath = "//button[@class='sign_in-exit']";
        private readonly string headlineArticleXPath = "//div[@data-entityid='container-top-stories#1']//h3";
        private readonly string secHeadlineArticlesXPath ="//div[contains(@class,'nw-c-top-stories__secondary-item')]//h3";
        private readonly string categoryXPath ="//div[@data-entityid='container-top-stories#1']//a/span[@aria-hidden='true']";
        private readonly string searchBarXPath = "//input[@id='orb-search-q']";
        private readonly string searcButtonXPath = "//button[@id='orb-search-button']";
        private readonly string coronavirusTabXPath = "//ul[contains(@class,'nw-c-nav__wide-sections')]//span[text()='Coronavirus']";

        public NewsPage(IWebDriver driver) : base(driver) { }

        public IReadOnlyCollection<IWebElement> ExitFromSingInMessageButton 
            => driver.FindElements(By.XPath(exitFromSingInMessageButtonXPath));
        
        public string HeadlineArticle 
            => driver.FindElement(By.XPath(headlineArticleXPath)).Text;

        public IReadOnlyCollection<IWebElement> SecondaryHeadlineArticles
            => driver.FindElements(By.XPath(secHeadlineArticlesXPath));

        public string MainHeadlineArticleCategory
            => driver.FindElement(By.XPath(categoryXPath)).Text;

        public IWebElement SearchBar => driver.FindElement(By.XPath(searchBarXPath));

        public IWebElement SearchButton => driver.FindElement(By.XPath(searcButtonXPath));

        public IWebElement CoronavirusTab => driver.FindElement(By.XPath(coronavirusTabXPath));
    }
}
