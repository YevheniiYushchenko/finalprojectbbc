﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace FinalProjectBBC.Pages
{
    public class BasePage
    {
        protected IWebDriver driver;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
        }
    }
}
