﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectBBC.Pages
{
    public class CoronavirusQuestionTab : BasePage
    {
        private string questionAreaXPath = "//textarea[@class='text-input--long']";
        private string nameAreaXPath = "//input[@aria-label='Name']";
        private string emailAreaXPath = "//input[@aria-label='Email address']";
        private string telephoneAreaXPath = "//input[@aria-label='Telephone number']";
        private string checkboxesXPath = "//input[@type='checkbox']";
        private string submitButtonXPath = "//div[@class='button-container']";
        private string errorMessageXPath = "//div[@class='input-error-message']";

        public CoronavirusQuestionTab(IWebDriver driver) : base(driver){ }

        public IWebElement QuestionArea => driver.FindElement(By.XPath(questionAreaXPath));

        public IWebElement NameArea => driver.FindElement(By.XPath(nameAreaXPath));

        public IWebElement EmailArea => driver.FindElement(By.XPath(emailAreaXPath));

        public IWebElement TelephoneArea => driver.FindElement(By.XPath(telephoneAreaXPath));

        public IReadOnlyCollection<IWebElement> Checkboxes 
            => driver.FindElements(By.XPath(checkboxesXPath));

        public IWebElement SubmitButton => driver.FindElement(By.XPath(submitButtonXPath));

        public string ErrorMessage => driver.FindElement(By.XPath(errorMessageXPath)).Text;

    }
}
