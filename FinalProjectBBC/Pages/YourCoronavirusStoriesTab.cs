﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectBBC.Pages
{
    public class YourCoronavirusStoriesTab : BasePage
    {
        private readonly string coronavirusQuestionTabXPath = "//h3[text()='Coronavirus: Send us your questions']/parent::a";

        public YourCoronavirusStoriesTab(IWebDriver driver) : base(driver) { }

        public IWebElement CoronavirusQuestionTab 
            => driver.FindElement(By.XPath(coronavirusQuestionTabXPath));
    }
}
