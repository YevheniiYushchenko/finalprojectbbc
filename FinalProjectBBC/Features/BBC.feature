﻿Feature: BBC
	In order to 
	As a user
	I want to try BBC site functionality 

@positiveTest
Scenario: Check that main article header equals to expected
	When I go to NewsPage
	Then main headline article is FBI busts 'plot' to abduct US governor

@positiveTest
Scenario: Check that headers of secondary articles equals to expected
	When I go to NewsPage
	Then actual headers of secondary articles contained in 
	| SecondaryArticles                                  |
	| Trump refuses to take part in virtual TV debate    |
	| Germany worried by spike in virus cases            |
	| 'I like to swim with my pet python'                |
	| Top Republican avoids White House over Covid-19    |
	| Algerian women protest over teen's rape and murder |

@positiveTest
Scenario: Check that header of first finded article by searching 
			category of main article header on NewsPage is equals to expected
	When I go to NewsPage
	And go to SearchPage by searching the main article category
	Then first finded article header is Woman arrested at US-Canada border for poison mailed to White House

@negativeTest
Scenario: Try to send a question form with empty Question field
	When I go to CoronavirusQuestionTab
	And send the form with values <Question> <Name> <Email> <Telephone>
	Then I see error message <Message>

Scenarios: 
	| Question       | Name  | Email          | Telephone     | Message                      |
	|                | Lorem | test@gmail.com | +123456789012 | can't be blank               |
	| dolor sit amet |       | test@gmail.com | +123456789012 | Name can't be blank          |
	| dolor sit amet | Lorem |                | +123456789012 | Email address can't be blank |
