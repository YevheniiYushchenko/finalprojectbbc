﻿using FinalProjectBBC.Pages;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using FluentAssertions;
using System.Linq;

namespace FinalProjectBBC.Steps
{
    [Binding]
    public class BBCSteps : BaseSteps
    {

        protected void GoToNewsPage()
        {
            HomePage.NewsButton.Click();
            WaitForPageLoad();
            SkipSignInMessage();
        }

        protected void GoToSearchPageBySearchQuery(string query)
        {
            NewsPage.SearchBar.SendKeys(query);
            NewsPage.SearchButton.Click();
            WaitForPageLoad();
        }

        protected void GoToCoronavirusTab()
        {
            NewsPage.CoronavirusTab.Click();
            WaitForPageLoad();
        }

        protected void GotoYourCoronavirusStoriesTab()
        {
            CoronavirusTab.YourCoronavirusStoriesTab.Click();
            WaitForPageLoad();
        }

        protected void GoToCoronavirusQuestionTab()
        {
            GoToCoronavirusTab();
            WaitForPageLoad();
            GotoYourCoronavirusStoriesTab();
            WaitForPageLoad();
            YourCoronavirusStoriesTab.CoronavirusQuestionTab.Click();
            WaitForPageLoad();
        }

        protected void SkipSignInMessage()
        {
            if (NewsPage.ExitFromSingInMessageButton.Count != 0)
                NewsPage.ExitFromSingInMessageButton.ElementAt(0).Click();
        }

        protected void SendCoronavirusQuestion(
            string question, string name, string email, string telephone)
        {
            CoronavirusQuestionTab.QuestionArea.SendKeys(question);
            CoronavirusQuestionTab.NameArea.SendKeys(name);
            CoronavirusQuestionTab.EmailArea.SendKeys(email);
            CoronavirusQuestionTab.TelephoneArea.SendKeys(telephone);
            foreach (IWebElement checkbox in CoronavirusQuestionTab.Checkboxes)
                checkbox.Click();
            CoronavirusQuestionTab.SubmitButton.Click();
            WaitTime();
        }

        [When(@"I go to NewsPage")]
        public void WhenIGoToNewsPage()
        {
            GoToNewsPage();
        }

        [When(@"go to SearchPage by searching the main article category")]
        public void WhenGoToSearcPageBySearchingTheMainArticleCategory()
        {
            GoToSearchPageBySearchQuery(NewsPage.MainHeadlineArticleCategory);
        }

        [When(@"I go to CoronavirusQuestionTab")]
        public void WhenIGoToCoronavirusQuestionTab()
        {
            GoToNewsPage();
            SkipSignInMessage();
            GoToCoronavirusQuestionTab();
        }

        [When(@"send the form with values (.*) (.*) (.*) (.*)")]
        public void WhenSendTheFormWithValues(
            string question, string name, string email, string telephone)
        {
            SendCoronavirusQuestion(question, name, email, telephone);
        }

        [Then(@"main headline article is (.*)")]
        public void ThenMainHeadlineArticleIs(string expected)
        {
            NewsPage.HeadlineArticle.Should().Be(expected);
        }

        [Then(@"actual headers of secondary articles contained in")]
        public void ThenActualHeadersOfSecondaryArticlesContainedIn(Table table)
        {
            NewsPage.SecondaryHeadlineArticles
                .Select(el => el.Text)
                .Should().Contain(
                table.Rows.Select(
                    row => row["SecondaryArticles"])
                .ToArray());
        }

        [Then(@"first finded article header is (.*)")]
        public void ThenFirstFindedArticleHeaderIs(string expected)
        {
            SearchPage.SearchResult.Should().Be(expected);
        }

        [Then(@"I see error message (.*)")]
        public void ThenISeeErrorMessage(string expected)
        {
            CoronavirusQuestionTab.ErrorMessage.Should().Be(expected);
        }

    }
}
