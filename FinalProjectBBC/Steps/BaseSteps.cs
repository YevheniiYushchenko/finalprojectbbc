﻿using FinalProjectBBC.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using TechTalk.SpecFlow;

namespace FinalProjectBBC.Steps
{
    public class BaseSteps
    {
        protected static IWebDriver driver;
        private readonly int TIME_TO_WAIT_FOR_PAGE_LOAD = 30;
        private readonly int TIME_TO_WAIT = 10;
        private readonly string BBC_URL = "https://www.bbc.com";

        protected HomePage HomePage;

        protected NewsPage NewsPage;

        protected SearchPage SearchPage;

        protected CoronavirusTab CoronavirusTab;

        protected CoronavirusQuestionTab CoronavirusQuestionTab;

        protected YourCoronavirusStoriesTab YourCoronavirusStoriesTab;

        protected void PagesGenerate()
        {
            HomePage = new HomePage(driver);
            NewsPage = new NewsPage(driver);
            SearchPage = new SearchPage(driver);
            CoronavirusTab = new CoronavirusTab(driver);
            CoronavirusQuestionTab = new CoronavirusQuestionTab(driver);
            YourCoronavirusStoriesTab = new YourCoronavirusStoriesTab(driver);
        }


        [Before]
        public void TestSetUp()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(BBC_URL);
            PagesGenerate();
        }

        [After]
        public void TearDown()
        {
            driver.Quit();
        }

        protected void WaitForPageLoad()
        {
            IWait<IWebDriver> wait = new WebDriverWait(
                driver, TimeSpan.FromSeconds(TIME_TO_WAIT_FOR_PAGE_LOAD));
            wait.Until(drivet => ((IJavaScriptExecutor)driver)
                .ExecuteScript("return document.readyState").Equals("complete"));
        }

        protected void WaitTime()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(TIME_TO_WAIT);
        }
    }
}
